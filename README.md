# Tel Aviv Pioneers - Open Brand Book
## 🚧 &nbsp;**IN CONSTRUCTION**
## **Created with&nbsp;💙&nbsp; by _Bar Yochai Bokovza_**
This is the repository of the Brand Book of the Israeli-American Football Team _Tel Aviv Pioneers_, which plays in the Israeli Football League.

This repository contains any asset that is used for both print and media content for the team:
- Logos
- Fonts
- Graphics for players
- Graphics for games
- Graphics for T-shirts
- etc...

In Addition, any meta assets (including **color pallettes**, and rules what to do and what not to do) are avaliable in the Brand Book itself (`.pdf` file, **not included yet**).

Rival teams are more than welcome to use any of the materials (with a request approval of course 😉)

## Tasks List
- Logos
  - Main Logos
  - Text Logos
- Graphical Assets
  - The Pioneer
  - Old Logo
  - Rival Teams Logos
- Player Mascot
- T-Shirts Graphics
- Flags
- Fonts
  - **Added to Repository** ☑️
  - Added to Brand Book
- InDesign Brand Books
